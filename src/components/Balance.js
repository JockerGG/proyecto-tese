import React, { Component } from 'react'
import DatePicker from 'react-datepicker'
import BalanceForm from './BalanceForm';
import { Link } from 'react-router-dom'
class Balance extends Component {

    constructor(props) {
        super(props)

        this.state = {
            officeName: "",
            sections: {
                ub: {
                    groupType: "sells",
                    inputs: [
                        { title: "Ventas", name: "sells", value: 0, operation: "positive" },
                        { title: "Costo de ventas", name: "sells", value: 0, operation: "negative" }
                    ],
                    result: { name: "Utilidad bruta: $", value: 0 }
                },
                ex: {
                    groupType: "expenses",
                    inputs: [
                        { title: "Gastos de ventas", name: "sells", value: 0, operation: "positive" },
                        { title: "Gastos de administración", name: "sells", value: 0, operation: "positive" }
                    ],
                    result: { name: "Utilidad opeación: $", value: 0 }
                },
                otherEx: {
                    groupType: "otherExpenses",
                    inputs: [
                        { title: "Gastos financieros", name: "sells", value: 0, operation: "positive" },
                        { title: "Productos financieros", name: "sells", value: 0, operation: "negative" },
                        { title: "Otros gastos", name: "sells", value: 0, operation: "positive" },
                        { title: "Otros prodcutos", name: "sells", value: 0, operation: "negative" }
                    ],
                    result: { name: "Utilidad antes de impuestos: $", value: 0 }
                }
            },
            ptu: 0,
            isr: 0,
            total: 0, 
            startDate: new Date(), 
            endDate: new Date(), 
        }
    }

    handleStartDate = (date) => {
        this.setState({
            startDate: date
        }) 
    }

    handleEndDate = (date) => {
        this.setState({
            endDate: date
        }) 
    }

    onOfficNameValueChange = ({target}) => {
        const { value } = target
        this.setState( { officeName: value } )
    }

    onValueChange = (name, value, index, sectionKey) => {
        let newSections = Object.assign({}, this.state.sections)
        newSections[sectionKey].inputs[index].value = value
        let result = 0
        newSections[sectionKey].inputs.forEach((element, index) => {
            switch (element.operation) {
                case "positive":
                    result = result + parseInt(element.value)
                    break
                case "negative":
                    result = result - parseInt(element.value)
                    break
            }
        })
        newSections[sectionKey].result.value = result

        switch (sectionKey) {
            case "ex":
                const up = parseInt(newSections[sectionKey].result.value)
                newSections[sectionKey].result.value = parseInt(newSections["ub"].result.value) - up
                break
            case "otherEx":
                const oe = parseInt(newSections[sectionKey].result.value)
                newSections[sectionKey].result.value = parseInt(newSections["ex"].result.value) - oe
                break
        }

        const newUe = parseInt(newSections["otherEx"].result.value)
        const isr = newUe * .30
        const ptu = newUe * .10
        const total = newUe - isr - ptu

        this.setState({ sections: newSections, isr, ptu, total })
    }

    render() {
        return (
            <div className="container mt-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><Link to="/">Home</Link></li>
                        <li class="breadcrumb-item active" aria-current="page">Estado de resultados</li>
                    </ol>
                </nav>
                <div className="row mt-2 justify-content-center">
                    <h1>Nuevo estado de resultados</h1>
                </div>

                <div className="row mt-3 justify-content-center">
                    <h3 className="font-weight-light">Para crear un estado de resultados es necesario que añadas valores a los siguientes campos: </h3>
                </div>

                <div className="row justify-content-center mt-3">
                    <div className="col-6">
                        <div className="form-group">
                            <label htmlFor="officeName">Nombre de la empresa</label>
                            <input type="text" className="form-control" id="officeName" name="officeName" placeholder="Ingresa el nombre de la empresa" onChange={this.onOfficNameValueChange} value={this.state.officeName} />
                        </div>
                    </div>
                    <div className="col-6 my-auto">
                        <div className="row justify-content-center">
                            <div className="col-6 text-right">Inicio del estado: </div>
                            <div className="col-6">
                                <DatePicker selected={this.state.startDate} onChange={this.handleStartDate} />
                            </div>
                            <div className="col-6 text-right mt-2">Fin del estado:</div>
                            <div className="col-6 mt-2">
                                <DatePicker selected={this.state.endDate} onChange={this.handleEndDate} />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <BalanceForm sections={this.state.sections} onChange={this.onValueChange} footers={this.state.footers} />
                </div>
                <div className="row justify-content-center">
                    <div className="col-8">
                        <h3 className="font-weight-light">ISR: ${this.state.isr}</h3>
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-8">
                        <h3 className="font-weight-light">PTU: ${this.state.ptu}</h3>
                    </div>
                </div>
                <div className="row justify-content-center mb-5">
                    <div className="col-8">
                        <h3 className="font-weight-light">Utilidad de ejercicio: ${this.state.total}</h3>
                    </div>
                </div>
            </div>
        )
    }
}

export default Balance