import React from 'react'

const CapitalTable = (props) => {
    const getValue = (type, options, action) => {
        return options.map(({ name, value }, index) => (
            <div className="row " key={index}>
                <div className="col-6">{name}</div>
                <div className="col-4">${value}</div>
                <div className="col-1">
                    <button className="remove-button" onClick={() => action(type, index)}>x</button>
                </div>
            </div>
        ))
    }

    return (
        <div className="container">
            <div className="row mt-5">
                <div className="col-6">
                    <h5 className="font-weight-light">Activo circulante</h5>
                    {getValue("AC", props.ac, props.onRemove)}
                </div>

                <div className="col-6">
                    <h5 className="font-weight-light">Pasivo a corto plazo</h5>
                    {getValue("PC", props.pc, props.onRemove)}
                </div>
            </div>

            <div className="row my-5 justify-content-center">
                <div className="col-6">
                    <h5 className="font-weight-light">Activo no circulante</h5>
                    {getValue("AN", props.an, props.onRemove)}
                </div>

                <div className="col-6">
                    <h5 className="font-weight-light">Pasivo a largo plazo</h5>
                    {getValue("PL", props.pl, props.onRemove)}
                </div>
            </div>
            <div className="dropdown-divider" />

            <div className="row my-5 justify-content-center">
                <div className="col-6">
                    <h4 className="font-weight-light">Total activo: ${props.ta}</h4>
                </div>
                <div className="col-6">
                    <h4 className="font-weight-light">Total pasivo: ${props.tp}</h4>
                </div>
            </div>

            <div className="dropdown-divider" />

            <div className="row mt-3 justify-content-right">
                <div className="col-6" />
                <div className="col-6">
                    <h5 className="font-weight-light">Capital</h5>
                </div>

                <div className="col-6" />
                <div className="col-6">
                    <h5 className="text-right font-italic">Capital Social: ${props.capital}</h5>
                </div>

                <div className="col-6" />
                <div className="col-6">
                    <h5 className="text-right font-italic">Resultado del ejercicio: ${props.result}</h5>
                </div>
                <div className="col-6" />
                <div className="col-6 dropdown-divider" />
                <div className="col-6" />
                <div className="col-12">
                    <h4 className="font-weight-light text-right">Total de capital social: ${props.total}</h4>
                </div>
            </div>

            <div className="row justify-content-right mb-5">
                <div className="col-6" />
                <div className="col-6 dropdown-divider" />
                <div className="col-6">
                    <h4 className="font-weight-light">Suma activo: ${props.ta}</h4>
                </div>
                <div className="col-6">
                    <h4 className="font-weight-light">Pasivo + capital: ${props.ppc}</h4>
                </div>
            </div>
        </div>
    )
}

export default CapitalTable