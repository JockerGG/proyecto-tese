import React from 'react'
import logo from '../assets/logo.jpg'
const Cover = () => {
    return (
        <div className="container mt-5 pt-2">
            <div className="row justify-content-center mt-5">
                <div className="col-12 mx-auto">
                    <h1 className="text-center font-weight-bold">Tecnológico de Estudios Superiores de Ecatepec</h1>
                    <h2 className="text-center mt-3 font-weight-bold">División de Ingeniería en Sistemas Computacionales</h2>
                </div>
                <div className="col-2 mx-auto my-auto py-4">
                    <img className="rounded d-flex mx-auto img-fluid" src={logo} />
                </div>
            </div>

            <div className="row justify-content-center mt-5">
                <div className="col-12 mx-auto">
                    <h3 className="text-center font-weight-normal">Contabilidad financiera</h3>
                    <h4 className="text-center mt-5 font-weight-light">Integrantes: </h4>
                    <div className="d-flex">
                        <ul className="mx-auto mt-3">
                            <li><h5 className="font-italic">Nancy Karina Jímenez Hernandez</h5></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Cover