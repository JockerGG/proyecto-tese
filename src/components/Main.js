import React, { Component } from 'react'
import logo from '../assets/logo.jpg'
import { withRouter } from 'react-router-dom'
import Cover from './Cover';
import Select from './Select';

class Main extends Component {
    constructor(props) {
        super(props)
        this.state = {
            showsCover: true
        }
    }
    async componentDidMount() {
        await this.setTimer()
        this.setState( { showsCover: false })
    }

    setTimer() {
        return new Promise((resolve) => {
            setTimeout(() => { resolve () }, 3000)
        })
    }
    render() {
        return(
            <div>
                {this.state.showsCover && <Cover/>}
                {!this.state.showsCover && <Select/>}
            </div>
        )
    }
}

export default withRouter(Main)