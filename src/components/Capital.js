import React, { Component } from 'react'
import CapitalTable from './CapitalTable';
import DatePicker from 'react-datepicker'
import { Link } from 'react-router-dom'
class Capital extends Component {
    constructor(props) {
        super(props)

        this.state = {
            //Activo circulante
            ac: [],
            //Activo no circulante
            an: [],
            //Pasivo a corto plazo
            pc: [],
            //Pasivo a largo plazo
            pl: [],
            //Pasivo + capital
            ppc: 0,
            //Total activo
            ta: 0,
            //Total pasivo
            tp: 0,
            //Capital social
            capital: 0,
            //Resultado del ejercicio
            result: 0,
            //Total de capital
            total: 0,
            //Nuevo registro
            record: {}, 
            startDate: new Date(), 
            endDate: new Date(), 
            officeName: ""
        }

    }

    onValueChange = ({ target }) => {
        const { name, value } = target
        let newRecord = Object.assign({}, this.state.record)
        switch (name) {
            case "recordName":
                newRecord.name = value
                this.setState({ record: newRecord })
                break
            case "recordValue":
                newRecord.value = value
                this.setState({ record: newRecord })
                break
            case "capital": {
                const newTotal = (parseInt(value) || 0) + parseInt(this.state.result)
                const ppc = parseInt(newTotal) + parseInt(this.state.tp)
                this.setState({ capital: (parseInt(value) || 0), total: newTotal , ppc})
                break
            }
            case "result":
                const newTotal = (parseInt(value) || 0) + parseInt(this.state.capital)
                const ppc = parseInt(newTotal) + parseInt(this.state.tp)
                this.setState({ result: (parseInt(value) || 0), total: newTotal, ppc})
                break
            case "officeName": 
                this.setState({ officeName: value })
            default:
                break
        }
    }

    getSumOfArray = (array) => {
        let sum = 0
        array.forEach(({ value }) => {
            sum = sum + parseInt(value)
        })
        return parseInt(sum)
    }

    addTo(type) {
        switch (type) {
            case "AC": {
                let ac = this.state.ac.concat(this.state.record)
                const ta = this.getSumOfArray(ac) + this.getSumOfArray(this.state.an)
                this.setState({ ac, ta })
                break
            }
            case "AN": {
                let an = this.state.an.concat(this.state.record)
                const ta = this.getSumOfArray(an) + this.getSumOfArray(this.state.ac)
                this.setState({ an, ta })
                break
            }
            case "PC": {
                let pc = this.state.pc.concat(this.state.record)
                const tp = this.getSumOfArray(pc) + this.getSumOfArray(this.state.pl)
                const ppc = parseInt(tp) + parseInt(this.state.capital)
                this.setState({ pc, tp, ppc })
                break
            }
            case "PL": {
                let pl = this.state.pl.concat(this.state.record)
                const tp = this.getSumOfArray(pl) + this.getSumOfArray(this.state.pc)
                const ppc = parseInt(tp) + parseInt(this.state.capital)
                this.setState({ pl, tp, ppc })
                break
            }
            default:
                break
        }
        this.setState({ record: { name: "", value: "" } })
    }

    removeTo = (type, index) => {
        console.log(index)
        switch (type) {
            case "AC": {
                let ac = this.state.ac
                ac.splice(index, 1)
                const ta = this.getSumOfArray(ac) + this.getSumOfArray(this.state.an)
                this.setState({ ac, ta })
                break
            }
            case "AN":{
                let an = this.state.an
                an.splice(index, 1)
                const ta = this.getSumOfArray(an) + this.getSumOfArray(this.state.ac)
                this.setState({ an, ta })
                break
            }
            case "PC":{
                let pc = this.state.pc
                pc.splice(index, 1)
                const tp = this.getSumOfArray(pc) + this.getSumOfArray(this.state.pl)
                const ppc = parseInt(tp) + parseInt(this.state.capital)
                this.setState({ pc, tp, ppc })
                break
            }
            case "PL":
                let pl = this.state.pl
                pl.splice(index, 1)
                const tp = this.getSumOfArray(pl) + this.getSumOfArray(this.state.pc)
                const ppc = parseInt(tp) + parseInt(this.state.capital)
                this.setState({ pl, tp, ppc })
                break
            default:
                break
        }
    }

    handleStartDate = (date) => {
        this.setState({
            startDate: date
        }) 
    }

    handleEndDate = (date) => {
        this.setState({
            endDate: date
        }) 
    }

    render() {
        return (
            <div className="container mt-5">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><Link to="/">Home</Link></li>
                        <li class="breadcrumb-item active" aria-current="page">Balance general</li>
                    </ol>
                </nav>
                <div className="row mt-2 justify-content-center">
                    <h1>Nuevo balance general</h1>
                </div>

                <div className="row mt-3 justify-content-center">
                    <h3 className="font-weight-light">Para crear un balance general es necesario que añadas valores a los siguientes campos: </h3>
                </div>

                <div className="row justify-content-center mt-3">
                    <div className="col-6">
                    <div className="form-group">
                                    <label htmlFor="officeName">Nombre de la empresa</label>
                                    <input type="text" className="form-control" id="officeName" name="officeName" placeholder="Ingresa el nombre de la empresa" onChange={this.onValueChange} value={this.state.officeName} />                                
                                </div>
                    </div>
                    <div className="col-6 my-auto">
                        <div className="row justify-content-center">
                            <div className="col-6 text-right">Inicio del balance: </div>
                            <div className="col-6">
                                <DatePicker selected={this.state.startDate} onChange={this.handleStartDate}/>
                            </div>
                            <div className="col-6 text-right mt-2">Fin del balance:</div>
                            <div className="col-6 mt-2">
                                <DatePicker selected={this.state.endDate} onChange={this.handleEndDate}/>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-4">
                        <div className="row mt-5">
                            <div className="col-12">
                                <div className="form-group">
                                    <label htmlFor="record">Añadir nuevo registro</label>
                                    <input type="text" className="form-control" id="record" name="recordName" placeholder="Ingresa el nombre del registro" onChange={this.onValueChange} value={this.state.record.name} />
                                    <input type="number" className="form-control mt-2" name="recordValue" placeholder="Ingresa una cantidad" onChange={this.onValueChange} value={this.state.record.value} />
                                    <button className="btn btn-outline-secondary dropdown-toggle d-block mx-auto mt-3" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Añadir a</button>
                                    <div className="dropdown-menu">
                                        <button className="dropdown-item" onClick={() => this.addTo("AC")}>Activo circulante</button>
                                        <button className="dropdown-item" onClick={() => this.addTo("AN")}>Activo no circulante</button>
                                        <div role="separator" className="dropdown-divider"></div>
                                        <button className="dropdown-item" onClick={() => this.addTo("PC")}>Pasivo corto plazo</button>
                                        <button className="dropdown-item" onClick={() => this.addTo("PL")}>Pasivo largo plazo</button>
                                    </div>
                                </div>
                            </div>

                            <div className="col-12">
                                <div className="form-group">
                                    <label htmlFor="capital">Editar capital</label>
                                    <input type="text" className="form-control" id="capital" name="capital" placeholder="Ingresa el monto de la capital" onChange={this.onValueChange} value={this.state.capital} />
                                </div>
                            </div>

                            <div className="col-12">
                                <div className="form-group">
                                    <label htmlFor="result">Editar resultado de ejercicio</label>
                                    <input type="text" className="form-control" id="result" name="result" placeholder="Ingresa el resultado del ejercicio" onChange={this.onValueChange} value={this.state.result} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-8">
                        <CapitalTable {...this.state} onRemove={this.removeTo} />
                    </div>
                </div>
            </div>
        )
    }
}

export default Capital