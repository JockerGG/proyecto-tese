import React from 'react'

const BalanceFormSingle = (props) => {
    const input = ({title, name, value}, index, sectionKey) => (
        <div className="form-group" key={index}>
            <label htmlFor="sells">{title}</label>
            <input type="number" 
                    className="form-control" 
                    name={name} placeholder="Ingresa una cantidad" 
                    value={value} 
                    onChange={
                        (event) => { props.onChange(event.target.name, event.target.value, index, sectionKey) } }/>
        </div>
    )
    return (
        <div>
            {props.options.map( (element, index) => input(element, index, props.sectionKey))}
            <div>
                <h3 className="text-right font-weight-light">{`${props.footer.name}${props.footer.value}`}</h3>
            </div>
        </div>
    )
}

export default BalanceFormSingle