import React from 'react'
import BalanceFormSingle from './BalanceFormSingle';

const BalanceForm = (props) => {
    console.log(JSON.stringify(props, null, 4))
    return (
        <div className="container mt-5">
            <div className="row justify-content-center">
                <div className="col-8">
                    <form>
                        { Object.keys(props.sections).map( (keySection, index) => (<BalanceFormSingle options={props.sections[keySection].inputs} onChange={props.onChange} footer={props.sections[keySection].result} sectionKey={keySection}/>))}
                        {/* <div className="form-group">
                            <label htmlFor="sells">Ventas</label>
                            <input type="number" className="form-control" id="sells" name="sells" placeholder="Ingresa una cantidad" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="sells-cost">Costos de ventas</label>
                            <input type="number" className="form-control" id="sells-cost" name="sells-cost" placeholder="Ingresa una cantidad" />
                        </div>
                        <div>
                            <h3 className="text-right font-weight-light">Utilidad bruta: $0</h3>
                        </div>

                        <div className="form-group">
                            <label htmlFor="expense-sells">Gastos de ventas</label>
                            <input type="number" className="form-control" id="expense-sells" name="expense-sells" placeholder="Ingresa una cantidad" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="expense-admin">Gastos de administración</label>
                            <input type="number" className="form-control" id="expense-admin" name="expense-admin" placeholder="Ingresa una cantidad" />
                        </div>
                        <div>
                            <h3 className="text-right font-weight-light">Utilidad de operación: $0</h3>
                        </div>

                        <div className="form-group">
                            <label htmlFor="expense-finance">Gastos financieros</label>
                            <input type="number" className="form-control" id="expense-finance" name="expense-finance" placeholder="Ingresa una cantidad" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="finance-products">Productos financieros</label>
                            <input type="number" className="form-control" id="expense-admin" name="finance-products" placeholder="Ingresa una cantidad" />
                        </div>
                        <div>
                            <h3 className="text-right font-weight-light">Utilidad de operación: $0</h3>
                        </div> */}
                        {/* <button type="submit" class="btn btn-primary">Submit</button> */}
                    </form>
                </div>
            </div>
        </div>
    )
}

export default BalanceForm