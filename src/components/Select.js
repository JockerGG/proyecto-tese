import React from 'react'
import { withRouter } from 'react-router-dom'

const Select = (props) => {
    const goToBalance = () => {
        props.history.push('/balance')
    }

    const goToCapital = () => {
        props.history.push('/capital')
    }
    return (
        <div className="container mt-5 pt-5">
            <div className="row mt-5">
                <div className="col-12">
                    <h1 className="text-center font-weight-bold">Bienvenido</h1>
                </div>
            </div>
            <div className="row mt-5">
                <div className="col-12">
                    <h1 className="text-center font-weight-light">Por favor elige una opción para continuar...</h1>
                </div>
            </div>
            <div className="row justify-content-center mt-5">
                <div className="col-4">
                    <button className="d-block btn btn-outline-primary btn-lg mr-1 mx-auto w-100" onClick={goToCapital}>Iniciar balance general</button>
                </div>

                <div className="col-4">
                    <button className="d-block btn btn-outline-secondary btn-lg mr-1 mx-auto w-100" onClick={goToBalance}>Iniciar estado de resultados</button>
                </div>
            </div>
        </div>
    )
}

export default withRouter(Select)