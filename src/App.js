import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Main from './components/Main';
import Balance from './components/Balance';
import Capital from './components/Capital';
function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Main} />
        <Route exact path="/balance" component={Balance} />
        <Route exact path="/capital" component={Capital} />
      </Switch>
    </Router>
  );
}

export default App;
